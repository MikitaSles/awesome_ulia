import random


def roll_dice():
    return random.randint(1, 6)


def draw_beetle():
    body = False
    head = False
    legs = 0
    antenna = 0
    eyes = 0
    mouth = 0
    rolls = 0
    roll_results = []

    while not (body and head and legs == 6 and antenna == 2 and eyes == 2 and mouth == 1):
        roll = roll_dice()
        rolls += 1
        roll_results.append(roll)

        if roll == 6 and not body:
            body = True
            print("Drawing the body.")
        elif roll == 5 and not head:
            head = True
            print("Drawing the head.")
        elif roll == 4 and body and legs < 6:
            legs += 1
            print(f"Drawing leg {legs}.")
        elif roll == 3 and head and antenna < 2:
            antenna += 1
            print(f"Drawing antenna {antenna}.")
        elif roll == 2 and head and eyes < 2:
            eyes += 1
            print(f"Drawing eye {eyes}.")
        elif roll == 1 and head and mouth < 1:
            mouth += 1
            print("Drawing the mouth.")

    print(f"Beetle complete in {rolls} rolls!")
    return rolls, roll_results


def save_results(scores):
    with open("beetle_results.txt", "w") as file:
        for player, score in scores.items():
            file.write(f"Player {player} - Number of rolls: {score[0]}\n")
            file.write(f"Roll results: {', '.join(str(roll) for roll in score[1])}\n\n")


def play_beetle_game(num_players):
    players_scores = {}

    for i in range(num_players):
        print(f"Player {i + 1} turn:")
        rolls, roll_results = draw_beetle()
        players_scores[i + 1] = (rolls, roll_results)
        print()

    winner = min(players_scores, key=lambda x: players_scores[x][0])
    print(f"Player {winner} wins with the fewest rolls!")
    save_results(players_scores)


num_players = int(input("Enter the number of players: "))
play_beetle_game(num_players)